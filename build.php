<?php

/*
 * Use http://www.php.net/manual/en/splfileobject.fputcsv.php to build the CSV file
 */

// pull in load_feed function
include 'inc/feed.php';
// set counter of number of SOAP calls
$count = 0;

// start at the beginning - get a list of all base categories
$categories = load_feed('category_list', 0);
$count++;

echo date("r");

echo "<ul>";

// loop though the list
foreach($categories as $category) {
	echo "<li>";

//	var_dump($category);
	echo "Category ID {$category->id}";

	// get a list of all sub categories
	$subcategories = load_feed('category_list', $category->id);
	$count++;

	echo "<ul>";

	// loop though list of subcategories
	foreach($subcategories as $subcategory) {

		echo "<li>";

//		var_dump($subcategory);
		echo "Subcategory ID {$subcategory->id}";

		// get a list of all products
		$products = load_feed('product_list', $subcategory->id, 0, 999999);
		$count++;

		echo "<ul>";

		// check for products
		if (!empty($products->product)) {

			// loop through list of products
			foreach($products as $product) {

				// check if we have product info (and not just a number of total records)
				if (isset($product->id)) {

					echo "<li>product";

					var_dump($product);

					// get product information
					$product_info = load_feed('product_view', $product->id);
					$count++;

//					var_dump($product_info);

					echo "</li>";

				}

			}

		}

		// check for bands
		if(!empty($products->bands->band)) {

			// loop though list of bands
			foreach($products->bands->band as $band) {
				// check if we have product info (and not just a number of total records)
				if (isset($band->product_id)) {

					echo "<li>band";

					var_dump($band);

					// get product information
					$product_info = load_feed('product_view', $band->product_id);
					$count++;

//					var_dump($product_info);

					echo "</li>";

				}
			}
		}

		echo "</ul>";

		echo "</li>";

	}

	echo "</ul>";

	echo "</li>";


}

echo "</ul>";

echo "<br />";
echo date("r");
echo "<br />";
echo $count;
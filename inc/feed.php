<?php
/**
 * Pulls in selected external feed
 *
 * @param string $type category_list, product_list or product_view
 * @param int $id ID of the relevant category or product
 * @param int $offset
 * @param int $limit
 * @return SimpleXML object
 */
function load_feed($type, $id = 0, $offset = 0, $limit = 10)
{
	/**
	 * Goldmark variables
	 */
	$user_id = 318;
	$base_url = 'http://www.goldmarkuk.com/xml/';
	$category_list_uri = $base_url . 'product/categories/';
	$category_list_all_uri = $base_url . 'product/all_categories/';
	$product_list_uri = $base_url . 'product/list_prod/{cat_id}/' . $user_id;
	$product_view_uri = $base_url . 'product/details/{product_id}/' . $user_id;
	$feed_url =	''; // used for return value

	/**
	 * Generate the correct feed URL
	 */
	switch ($type)
	{
		case 'category_list':
			$feed_url = $category_list_uri . (int) $id;
			break;
		case 'category_list_all':
			$feed_url = $category_list_all_uri . (int) $id;
			break;
		case 'product_list':
			$feed_url = str_replace('{cat_id}', (int) $id, $product_list_uri);
			$feed_url = $feed_url . '?offset=' . $offset . '&limit=' . $limit;
			break;
		case 'product_view':
			$feed_url = str_replace('{product_id}', (int) $id, $product_view_uri);
			$feed_url .= '?' . $_SERVER['QUERY_STRING'];
			break;
	}

	/**
	 * Return SimpleXML object
	 */
	return simplexml_load_file($feed_url, 'SimpleXMLElement', LIBXML_NOCDATA);
}
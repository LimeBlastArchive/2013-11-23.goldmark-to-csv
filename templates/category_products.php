<h1>Category Products</h1>

<ul>

	<?php if (!empty($products->product)): ?>
		<?php foreach($products->product as $product): // loop products ?>
			<li>
				<h3>
					<a href="view.php?product=<?=$product->id?>">
						<?=$product->product_code?>
					</a> (Product)
				</h3>
				<? var_dump($product); ?>
			</li>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if(!empty($products->bands->band)): ?>
		<?php foreach($products->bands->band as $band): // loop bands ?>
			<li>
				<h3>
					<a href="view.php?product=<?=$band->product_id?>">
						<?=$band->product_code?>
					</a> (Band)
				</h3>
				<? var_dump($band); ?>
			</li>
		<?php endforeach; ?>
	<?php endif; ?>

</ul>

<?php ob_start(); // start pagination ?>
	<?php if ($limit < $total_records): ?>
		<p style="text-align: center;">
			<?= ($offset / $limit) + 1; // current page ?>
			of
			<?= ceil($total_records / $limit); // total number of pages ?>

			<?php if($offset >= $limit): // prev link ?>
				<a href="view.php?category=<?=$category_id?>&offset=<?=$offset - $limit?>">Previous</a>
			<?php endif; ?>

			<?php if($offset + $limit < $total_records): // next link ?>
				<a href="view.php?category=<?=$category_id?>&offset=<?=$offset + $limit?>">Next</a>
			<?php endif; ?>

			<a href="view.php?category=<?=$category_id?>&offset=0&limit=<?=$total_records?>">View All</a>
		</p>
	<?php endif; ?>
<?php $pagination = ob_get_contents(); // get contents of buffer to use again?>
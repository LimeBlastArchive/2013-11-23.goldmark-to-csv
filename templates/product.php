<?php

// Used for the labels of the product options
$postvar_names = array(
	'metal' => 'Metal Type',
	'carat' => 'Stone Size',
	'colour' => 'Stone Colour',
	'clarity' => 'Stone Clarity',
	'ringsize' => 'Ring Size',
	'wedding_band_id' => 'Wedding Band',
	'finish' => 'Finish',
);

var_dump($product);
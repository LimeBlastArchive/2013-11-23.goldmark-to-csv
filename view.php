<?php

// pull in load_feed function
include 'inc/feed.php';

/**
 * Check for product or category id
 */
$product_id = (isset($_GET['product'])) ? (int) $_GET['product'] : false;
$category_id = (isset($_GET['category'])) ? (int) $_GET['category'] : 0;
$offset = (isset($_GET['offset'])) ? (int) $_GET['offset'] : 0;
$limit = (isset($_GET['limit'])) ? (int) $_GET['limit'] : 10;

/**
 * Establish which page we're loading
 */
if ($product_id) // display selected product
{
	$product = load_feed('product_view', $product_id);
	include('templates/product.php');
}
else // display selected category
{
	$categories_all = load_feed('category_list_all', $category_id);
	$categories = load_feed('category_list', $category_id);

	// find the name for each category
	$categories_names = array(
		'0' => 'Biagio Diamonds' // set default
	);
	foreach($categories_all as $category)
	{

		$categories_names[(int)$category->id] = (string)$category->name;
		foreach($category->sub_categories->category as $sub_category)
		{
			$categories_names[(int)$sub_category->id] = (string)$sub_category->name;
		}
	}

	// check for sub-categories, because if they exist, no valid products will
	if ((count($categories)) > 0)
	{
		// show products list
		include('templates/category_subcategories.php');
	}
	else
	{
		// show categories list
		$products = load_feed('product_list', $category_id, $offset, $limit);
		$total_records = !empty($products->record_info->total_records) ? (int) $products->record_info->total_records : 0;
		include('templates/category_products.php');
	}
}